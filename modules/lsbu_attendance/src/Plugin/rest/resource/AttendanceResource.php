<?php
/**
 * @file
 * Attendance REST resource.
 */

namespace Drupal\lsbu_attendance\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Site\Settings;

/**
 * @RestResource(
 *   id = "lsbu_attendance",
 *   label = @Translation("List Attendance"),
 *   uri_paths = {
 *     "canonical" = "/api/lsbu_attendance"
 *   }
 * )
 */
class AttendanceResource extends ResourceBase {

  /**
   * Current logged in user.
   *
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('ccms_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Rest endpoint to retrieve tweets from endpoint.
   *
   * @return \Drupal\rest\ResourceResponse
   */
  public function get() {
    if (!$this->currentUser->isAuthenticated()) {
      throw new AccessDeniedHttpException('Invalid credentials.');
    }

    $disableCache = new CacheableMetadata();
    $disableCache->setCacheMaxAge(0);

    $data = $this->getEndpoint() ?: $this->getAttendance();
    return (new ResourceResponse($data))->addCacheableDependency($disableCache);
  }

  /**
   * Gets the data from the endpoint.
   *
   * @return array
   */
  private function getEndpoint() {
    $endpoint = strtr(Settings::get('soe_endpoint'), [
      '!key' => Settings::get('soe_key'),
      '!email' => $this->currentUser->getEmail(),
    ]);

    try {
      $client = \Drupal::httpClient();
      $request = $client->get($endpoint);
      $response = $request->getBody();
      $data = @json_decode($response, true);
      return is_array($data) ? $data : null;
    } catch (\GuzzleHttp\Exception\ClientException $exception) {
      return null;
    }
  }


  /**
   * Gets the attendance.
   *
   * @return array
   */
  private function getAttendance() {
    $query = <<<SQL
SELECT r.unitcode, r.weekcount, r.room, r.date, rt.attend, r.time, r.day, r.semester, rn.unitname
  FROM R10_registers r, R10_register_ticks rt, R10_studentnames rs, R10_unitnames rn
WHERE rs.email = :email
  AND rt.studentnumber = rs.studentnumber
  AND r.regid = rt.regid
  AND r.unitcode = rn.unitcode;
SQL;

    $connection = \Drupal\Core\Database\Database::getConnection('default','attendance');
    $results = $connection->query($query, array(
      'email' => $this->currentUser->getEmail(),
    ));
    return $this->parseResults($results);
  }

  /**
   * Parses the attendance results.
   *
   * @return array
   */
  private function parseResults($results) {
    $list = array();
    foreach ($results as $result) {
      $attendance = str_split(decbin($result->attend));
      while (count($attendance) < $result->weekcount) {
        array_unshift($attendance, 0);
      }
      $list[] = array(
        'room' => $result->room,
        'date' => $result->date,
        'time' => $result->time,
        'day' => $result->day,
        'semester' => $result->semester,
        'weekcount' => $result->weekcount,
        'moduleCode' => $result->unitcode,
        'moduleName' => $result->unitname,
        'attPercentage' => $this->getAttendancePercentage($attendance, $result->weekcount),
        'attendance' => array_map(array($this, 'attendanceOutput'), $attendance),
      );
    }
    return $list;
  }

  /**
   * Divide number of attended lessons with expected number of lessons and times by 100.
   *
   * @param $num_attended | array
   * @param $expected | int
   * @return float
   */
  private function getAttendancePercentage($num_attended, $expected) {
    $attended = 0;
    foreach ($num_attended as $a) {
      if ($a == '1') {
        $attended++;
      }
    }
    $percentage = $attended/$expected*100;
    return round($percentage);
  }

  /**
   * Maps the attendance into something the frontend can understand.
   *
   * @param string $value
   *   Value of the attendance as it's in the database.
   *
   * @return string
   */
  public function attendanceOutput($value) {
    return ($value) ? 'Y' : 'N';
  }
}

?>
