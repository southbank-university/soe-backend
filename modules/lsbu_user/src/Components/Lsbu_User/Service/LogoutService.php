<?php
namespace Drupal\lsbu_user\Components\Lsbu_User\Service;

use Drupal\lsbu_user\REST_Gateway\Http\LsbuUserResponseHandler;

class LogoutService {
  private $responder;

  public function __construct(LsbuUserResponseHandler $responseHandler) {
    $this->responder = $responseHandler;
  }

  public function logoutUser($uid) {
    // TODO: For now logging out will be done on client side only.
  }
}