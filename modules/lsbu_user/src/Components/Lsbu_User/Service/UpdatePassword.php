<?php
namespace Drupal\lsbu_user\Components\Lsbu_User\Service;

use Drupal\lsbu_user\REST_Gateway\Http\LsbuUserResponseHandler;
use Drupal\user\Entity\User;

class UpdatePassword {
  private $resetPassword;
  private $responseHandler;
  
  public function __construct(ResetPassword $resetPassword, LsbuUserResponseHandler $responseHandler) {
    $this->resetPassword = $resetPassword;
    $this->responseHandler = $responseHandler;
  }
  
  public function RequestReset($mail) {
    $user = user_load_by_mail($mail);
    if ($user) {
      // Set verification code for user
        $this->setResetToken($mail);
      // Send email to the for account activation
      _user_mail_notify('password_reset', $user);
      
      return $this->responseHandler->onPassResetEmailSent();
    }
    
    return $this->responseHandler->onPassResetEmailSentError();
  }


  public function UpdateUserPassword($uid, $code, $newpass) {
    if ($this->resetPassword->verifyResetToken($uid, $code)) {
      $user = User::load($uid);
      if ($user) {
        $user->setPassword($newpass);
        $user->save();
        return $this->responseHandler->onPassResetSuccess();
      }
      
      return $this->responseHandler->onPassResetError();
    }
  }
  
  private function setResetToken($email) {
    $this->resetPassword->loadUser($email);
    return $this->resetPassword->storePassResetToken();
  }
}
