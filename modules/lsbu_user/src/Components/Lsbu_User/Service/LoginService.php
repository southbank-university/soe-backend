<?php
namespace Drupal\lsbu_user\Components\Lsbu_User\Service;

use Drupal\Core\Password\PhpassHashedPassword;
use Drupal\lsbu_user\REST_Gateway\Http\LsbuUserResponseHandler;
use Drupal\user\Entity\User;

class LoginService {
  private $responder;

  public function __construct(LsbuUserResponseHandler $responseHandler) {
    $this->responder = $responseHandler;
  }

  public function loginUser($data) {
    $u = User::load(1);
    $user = user_load_by_mail($data['email']);
    if ($user) {
      if ($this->verifyPassword($user, $data['pass'])) {
        if ($user->isActive()) {
          user_login_finalize($user);
          return $this->prepareLoginResponse();
        }
        return $this->responder->onAccountNotActive();
      }
      return $this->responder->onInvalidPassword();
    }
    return $this->responder->onLoginErrorResponse();
  }


  private function verifyPassword($user, $pass) {
    $original_pass = $user->get('pass')->value;
    $checker = new PhpassHashedPassword(1);

    return $checker->check($pass, $original_pass);
  }

  // TODO: Check if the user if already logged in

  private function prepareLoginResponse() {
    $user = \Drupal::currentUser();
    $loginResponse =  array(
      "username" => $user->getDisplayName(),
      "email" => $user->getEmail(),
      "uid" => $user->id(),
    );

    return $this->responder->onLoginSuccessResponse($loginResponse);
  }

}