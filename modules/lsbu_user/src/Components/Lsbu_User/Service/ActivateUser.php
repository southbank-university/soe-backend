<?php
namespace Drupal\lsbu_user\Components\Lsbu_User\Service;

use Drupal\user\Entity\User;
use Drupal\lsbu_user\Components\Lsbu_User\Service\ActivateAccount;
use Drupal\lsbu_user\REST_Gateway\Http\LsbuUserResponseHandler;

class ActivateUser {
  private $activateAccount;
  private $responseHandler;

  public function __construct(ActivateAccount $activateAccount, LsbuUserResponseHandler $responseHandler) {
    $this->activateAccount = $activateAccount;
    $this->responseHandler = $responseHandler;
  }

  public function ActivateUserAccount($uid, $code) {
    if ($this->activateAccount->verifyUser($uid, $code)) {
      $user = User::load($uid);
      if ($user->isBlocked()) {
        $user->activate();
        $user->save();
        return $this->responseHandler->onAccountActivateSuccess();
      }

      return $this->responseHandler->onAccountAlreadyActive();
    }

    return $this->responseHandler->onAccountActivateError();
  }

}