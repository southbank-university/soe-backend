<?php
namespace Drupal\lsbu_user\REST_Gateway\Http;

use Drupal\lsbu_user\REST_Gateway\Service\LsbuUserService;
use Symfony\Component\HttpFoundation\Request;

/**
 * The url will be updated once we moved to cloud server.
 * Base url: http://soeurl
 * User related ajax calls:
 * url => http://soeurl/lsbu/soe?entity=user& \
 *            type={login, register, user_activate, user_delete, user_request_reset_pass, user_reset_pass}
 *
 * Class ElephantRequestHandler
 * @package Drupal\lsbu_user\REST_Gateway\Http
 */

class LsbuUserRequestHandler extends LsbuUserService {
  private $intentEntity;
  private $intentType;

  public function getUserRequest(Request $request) {
    $this->intentEntity = $request->query->get('entity');
    $this->intentType = $request->query->get('type');
     
    return $this->prepareIntent($request);
  }

  private function prepareIntent($request) {
    switch ($this->intentEntity) {
      case 'user': return self::getUserServiceType($this->intentType, $request);
    }
  }

}