<?php

namespace Drupal\lsbu_twitter\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\lsbu_twitter\TwitterConfig;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\lsbu_twitter\TwitterApi;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * @RestResource(
 *   id = "lsbu_twitter",
 *   label = @Translation("List tweets"),
 *   uri_paths = {
 *     "canonical" = "/api/lsbu_twitter/{method}/{account}",
 *     "https://www.drupal.org/link-relations/create" = "/api/lsbu_twitter/{method}/{account}"
 *   }
 * )
 */
class TwitterResource extends ResourceBase {

  protected $currentUser;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('ccms_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Rest endpoint to retrieve tweets from endpoint.
   *
   * @param string $method
   * @param string $account
   * @return \Drupal\rest\ResourceResponse
   */
  public function get($method, $account) {
    //Check the user has auth
    if (!$this->currentUser->isAuthenticated()) {
      throw new AccessDeniedHttpException('Invalid credentials.');
    }

    /**
     * Method is what we are intending to receive. Since we only support list atm
     * it's not necessary to check. But in the future we should have a list
     * of supported methods and then handlers for those.
     */


    if (!$this->isAccountSupported($account)) {
      return new ResourceResponse("Unsupported account.", 500);
    }

    $api = new TwitterApi();
    $result = $api->loadTweets($account);
    $response = NULL;

    if (!$result->isSuccess()) {
      $response = new ResourceResponse($result->getMessage(), 500);
    }
    else {
      $tweets = $result->getData();
      $response = new ResourceResponse($this->sanitizeTweets($tweets));

    }

    return $this->updateResponseCaching($response);
  }

  /**
   * Is this account supported by our endpoint?
   *
   * @param string $account
   * @return boolean
   */
  public function isAccountSupported($account) {
    $supportedAccounts = TwitterConfig::instance()->getSupportedAccounts();
    foreach ($supportedAccounts as $supportedAccount) {
      //Equals ignore case the strings
      if (strcasecmp($account, $supportedAccount) == 0) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Returns $response with updated caching.
   * If $cache not specified returns $response without caching.
   *
   * @param ResourceResponse $response
   * @param array|CacheableDependencyInterface $cache
   * @return ResourceResponse
   */
  public function updateResponseCaching($response, $cache = NULL) {
    if (!$cache) {
      $cache = array(
        '#cache' => array(
          'max-age' => 0,
        )
      );
    }
    $response->addCacheableDependency($cache);
    return $response;
  }

  /**
   * Converts nodes into format front-end can understand.
   *
   * @param $tweets Node[]
   * @return array
   */
  public function sanitizeTweets($tweets) {
    $sanitized = [];
    foreach ($tweets as $tweet) {
      array_push($sanitized, array(
        'id' => $tweet->getTitle(),
        'created_at' => $tweet->get('field_created_at')->get(0)->getString(),
        'content' => $tweet->get('field_text')->get(0)->getString()
      ));
    }
    return $sanitized;
  }

}

?>
