<?php

namespace Drupal\lsbu_twitter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\Entity\Node;

/**
 * Class TwitterApi
 * @package Drupal\lsbu_twitter
 */
class TwitterApi {

  const API_URL = "https://api.twitter.com/";

  /**
   * Datetime format used by twitter API used to parse Datetime object in API requests
   * http://php.net/manual/en/function.date.php
   * eg. Wed Aug 29 17:12:58 +0000 2012
   *
   * @var string
   */
  const DATETIME_FORMAT = "D M d H:i:s O Y";

  /**
   * Loads cached tweets after polling the Twitter API for new tweets.
   *
   * @param string $account
   * @return Result
   */
  public function loadTweets($account) {
    //This checks for recent tweets and then caches them if any are found.
    //If this fails we return with it's error result.
    $result = $this->refreshTweets($account);
    if (!$result->isSuccess()) {
      return $result;
    }
    //Queries 10 most recent tweets.
    $data = $this->getTweetNodes(10);
    return Result::success($data);
  }

  /**
   * Poll the Twitter API for any new tweets since last tweet cached.
   * If run when there are no tweets cached, polls 10 most recent.
   *
   * @param string $account
   * @return \Drupal\lsbu_twitter\Result
   */
  public function refreshTweets($account) {
    if (!TwitterConfig::instance()->isConsumerKeysValid()) {
      return Result::fail("API Key or Secret missing for Twitter Feed.");
    }

    $client = new TwitterApiClient();
    $lastId = TwitterConfig::instance()->getLatestTweetId();
    $result = $client->retrieveLatestTweets($account, 10, $lastId);
    if (!$result->isSuccess()) {
      return $result;
    }
    $tweets = $result->getData();

    //Cache all tweets
    $latestId = '0';
    foreach ($tweets as $tweet) {

      $id = $tweet->id;
      $this->saveTweetNode($tweet);
      if ($id>$latestId) {
        //if (bcsub($id, $latestId) > 0) {
        $latestId = $id;
      }
    }

    //We store the latest tweet for next time.
    if (!$lastId || strlen($lastId) == 0) {
      if ($latestId>$lastId) {
      // if (bcsub($latestId, $lastId) > 0) {
        TwitterConfig::instance()->setLatestTweetId($latestId);
      }
    }

    return Result::success($tweets);
  }

  /**
   * Converts the raw tweet data into a node.
   * Will not cache the same tweet twice.
   *
   * @param mixed $tweet
   * @return Node|null
   */
  public function saveTweetNode($tweet) {
    $node = $this->getTweetNode($tweet->id);
    if ($node) {
      return $node;
    }

    try {
      $created_at = DrupalDateTime::createFromFormat(self::DATETIME_FORMAT, $tweet->created_at);
    } catch (\Exception $e) {
      return NULL;
    }

    $node = Node::create([
      "type" => "tweet",
      "title" => $tweet->id,
      "field_text" => $tweet->text,
      "field_created_at" => $created_at->format(DATETIME_DATETIME_STORAGE_FORMAT),
      "field_id" => $tweet->id,
      "field_user" => 0
    ]);
    $node->save();
    return $node;
  }

  /**
   * Queries all nodes for tweets which match $id
   * TODO: Abstract into queryCachedTweet()?
   * TODO: There is code that does the same thing in this method and getCachedTweets()
   *
   * @param $id
   * @return Node|null
   */
  public function getTweetNode($id) {
    $query = $this->createTweetNodeQuery()
      ->condition('title', $id);
    $entity_ids = $query->execute();

    if (empty($entity_ids)) {
      return NULL;
    }

    $node = Node::load($entity_ids[0]);
    $node = array_values($node)[0];

    if (!$this->validateTweetNode($node)) {
      $this->deleteTweetNode($node);
      return NULL;
    }
    return $node;
  }


  /**
   * Query last $count most recent tweets.
   *
   * @param integer $count
   * @return Node[]
   */
  public function getTweetNodes($count) {
    $query = $this->createTweetNodeQuery()
      ->range(0, $count);
    $entity_ids = $query->execute();

    $nodes = Node::loadMultiple($entity_ids);
    $returnNodes = [];

    foreach ($nodes as $id => $node) {
      if ($this->validateTweetNode($node)) {
        array_push($returnNodes, $node);
      } else {
        $this->deleteTweetNode($node);
      }
    }
    return $returnNodes;
  }

  /**
   * Constructs an entity query that targets tweets.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   */
  public function createTweetNodeQuery() {
    $query = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'tweet')
      ->sort('title');
    return $query;
  }

  /**
   * Deletes cached tweet node
   * TODO: Methods that take node IDs and methods that take actual nodes are a little ambiguous.
   * TODO: In the scenario that a cached tweet is deleted ATM it is not possible to re-acquire because we only poll recent tweets. Possible fix 1) decrement latest ID to this tweet, 2) don't delete invalid tweets, simply force them to be updated from Twitter API
   *
   * @param Node $node
   */
  private function deleteTweetNode($node) {
    if ($this->validateTweetNode($node)) {
      $node->delete();
    }
  }

  /**
   * Checks whether node is valid, may be deleted if FALSE.
   *
   * @param Node $node
   * @return boolean
   */
  public function validateTweetNode($node) {
    if (!$node || $node->get("field_created_at")->isEmpty()) {
      //TODO: This should be serious if this ever happens.
      return FALSE;
    }
    return TRUE;
  }

}

?>
