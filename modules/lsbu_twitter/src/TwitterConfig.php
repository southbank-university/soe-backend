<?php

namespace Drupal\lsbu_twitter;

/**
 * Class TwitterConfig
 * @package Drupal\lsbu_twitter
 */
class TwitterConfig {

  private static $instance;

  /**
   * @return \Drupal\lsbu_twitter\TwitterConfig
   */
  public static function instance() {
    if (!self::$instance) {
      return self::$instance = new TwitterConfig();
    }
    return self::$instance;
  }

  private $config;

  private function __construct() {
    $this->config = \Drupal::configFactory()
      ->getEditable("lsbu_twitter.settings");
  }

  /**
   * @return string|null
   */
  public function getApiUrl() {
    return $this->config->get("twitter_feed_api_url");
  }

  /**
   * @return boolean
   */
  public function isApiUrlValid() {
    $apiUrl = $this->getApiUrl();
    return $this->isValueValid($apiUrl) && stringStartsWith("https://",$apiUrl);
  }

  /**
   * @return string|null
   */
  public function getConsumerKeyPublic() {
    return $this->config->get("twitter_feed_api_key");
  }

  /**
   * @return string|null
   */
  public function getConsumerKeySecret() {
    return $this->config->get("twitter_feed_api_secret");
  }

  /**
   * @return array|null
   */
  public function getSupportedAccounts() {
    return explode(',',$this->config->get("twitter_feed_supported_accounts"));
  }

  /**
   * @return bool
   */
  public function isConsumerKeysValid() {
    return $this->isValueValid($this->getConsumerKeyPublic())
      && $this->isValueValid($this->getConsumerKeySecret());
  }

  /**
   * @return string|null
   */
  public function getLatestTweetId() {
    return $this->config->get("twitter_feed_last_id");
  }

  /**
   * @param string $id
   */
  public function setLatestTweetId($id) {
    $this->config->set("twitter_feed_last_id", $id);
    $this->save();
  }

  /**
   * Save config.
   */
  private function save() {
    $this->config->save();
  }

  /**
   * @param mixed $value
   * @return bool
   */
  private function isValueValid($value) {
    return $value != NULL && $value != 'null';
  }

  /**
   * @return bool
   */
  public function isDebug() {
    $debug = $this->config->get('debug');
    return $debug || $debug == 'true';
  }

}
?>