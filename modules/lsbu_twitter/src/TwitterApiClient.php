<?php
/**
 * Created by PhpStorm.
 * User: 09jwa
 * Date: 16/01/2017
 * Time: 12:42 AM
 */

namespace Drupal\lsbu_twitter;


use GuzzleHttp\Client;

/**
 * Handles all external API requests.
 *
 * Class TwitterApiClient
 * @package Drupal\lsbu_twitter
 */
class TwitterApiClient {

  /**
   * @var Client
   */
  private $httpClient;

  public function __construct() {
    $this->httpClient = new Client();
  }

  /**
   * Constructs request options for guzzle client.
   *
   * @param array $headers
   * @param array $formParams
   * @return array
   */
  private function createOptions($headers, $formParams = []) {
    $options = [
      'headers' => $headers,
      'timeout' => 10,
      'form_params' => $formParams,
      'referer' => TRUE,
      'allow_redirects' => TRUE,
      'decode_content' => 'gzip',
    ];
    return $options;
  }

  public function getApiUrl() {
    return TwitterConfig::instance()->getApiUrl();
  }

  /**
   * Request oAuth token from twitter API.
   *
   * @return Result
   */
  public function generateToken() {
    $privateKey = TwitterConfig::instance()->getConsumerKeySecret();
    $publicKey = TwitterConfig::instance()->getConsumerKeyPublic();

    $encoded_key = base64_encode("$publicKey:$privateKey");
    $options = $this->createOptions([
      'Authorization' => "Basic $encoded_key",
    ], [
      'grant_type' => 'client_credentials'
    ]);

    $token = NULL;

    try {
      $res = $this->httpClient->post($this->getApiUrl() . 'oauth2/token', $options);

      $body = json_decode($res->getBody()->getContents());
      $token = "{$body->token_type} $body->access_token";
    } catch (\Exception $e) {
      if ($e->getCode() == 403) {
        return Result::error($e, "Invalid API keys: " . $e->getMessage());
      }
      return Result::error($e);
    }
    return Result::success($token);
  }

  /**
   * Returns latest tweets from a ID specified.
   * If $lastId is not specified simply returns latest $count tweets.
   *
   * @param string $account
   * @param integer $count
   * @param string $lastId
   * @return \Drupal\lsbu_twitter\Result
   */
  public function retrieveLatestTweets($account, $count, $lastId = '') {
    $result = $this->generateToken();
    if (!$result->isSuccess()) {
      return $result;
    }
    $token = $result->getData();

    $options = $this->createOptions([
      'Authorization' => $token
    ]);

    try {
      // Now get the tweets.
      // https://dev.twitter.com/rest/reference/get/statuses/user_timeline
      $query = [
        'screen_name' => $account,
        'count' => $count
      ];
      if ($lastId && strlen($lastId) > 0) {
        $query['since_id'] = $lastId;
      }
      $query = http_build_query($query);

      // Fetches the tweets.
      $res = $this->httpClient->get($this->getApiUrl() . "1.1/statuses/user_timeline.json?$query", $options);

      //Parse them
      $tweets = json_decode($res->getBody()->getContents());

    } catch (\Exception $e) {
      return Result::error($e);
    }
    return Result::success($tweets);
  }

}