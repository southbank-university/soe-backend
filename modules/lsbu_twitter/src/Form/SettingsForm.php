<?php

namespace Drupal\lsbu_twitter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lsbu_twitter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'lsbu_twitter.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('lsbu_twitter.settings');

    $form['twitter_feed_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Twitter API Public Key'),
      '#default_value' => $config->get('twitter_feed_api_key'),
    );

    $form['twitter_feed_api_secret'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Twitter API Secret Key'),
      '#default_value' => $config->get('twitter_feed_api_secret'),
    );

    $form['twitter_feed_last_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Last tweet collected'),
      '#default_value' => $config->get('twitter_feed_last_id'),
    );

    $form['twitter_feed_api_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('API Url (must be https)'),
      '#default_value' => $config->get('twitter_feed_api_url'),
    );

    $form['twitter_feed_supported_accounts'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Accounts supported by API, split with ","'),
      '#default_value' => $config->get('twitter_feed_supported_accounts')
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = \Drupal::service('config.factory')
      ->getEditable('lsbu_twitter.settings')
      ->set('twitter_feed_api_key', $form_state->getValue('twitter_feed_api_key'))
      ->set('twitter_feed_api_secret', $form_state->getValue('twitter_feed_api_secret'))
      ->set('twitter_feed_api_url', $form_state->getValue('twitter_feed_api_url'))
      ->set('twitter_feed_supported_accounts', $form_state->getValue('twitter_feed_supported_accounts'))
      ->set('twitter_feed_last_id', $form_state->getValue('twitter_feed_last_id'));

    $config->save();

    parent::submitForm($form, $form_state);
  }
}