<?php

namespace Drupal\lsbu_twitter;

/**
 * Class Result
 * @package Drupal\lsbu_twitter
 */
class Result {

  private $success;
  private $data;
  private $message;
  private $exception;

  protected function __construct($success, $data, $message, $exception) {
    $this->success = $success;
    $this->data = $data;
    $this->message = $message;
    $this->exception = $exception;
  }

  /**
   * Represents a successful operation holding data generated.
   *
   * @param mixed $data
   * @param string $message
   * @return \Drupal\lsbu_twitter\Result
   */
  public static function success($data, $message = NULL) {
    return new Result(TRUE, $data, $message, NULL);
  }

  /**
   * Represents a failure state with a message.
   *
   * @param string $message
   * @return \Drupal\lsbu_twitter\Result
   */
  public static function fail($message) {
    return new Result(FALSE, NULL, $message, NULL);
  }

  /**
   * Represents a failure state with an exception.
   * Optional message to override exception message.
   *
   * @param \Exception $exception
   * @param string $message
   * @return \Drupal\lsbu_twitter\Result
   */
  public static function error($exception, $message = NULL) {
    return new Result(FALSE, NULL, $message, $exception);
  }

  /**
   * Was this operation successful?
   *
   * @return boolean
   */
  public function isSuccess() {
    return $this->success;
  }

  /**
   * The data held by this object, will be NULL if !isSuccess()
   *
   * @return mixed|null
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Is there any data attached to this result?
   *
   * @return bool
   */
  public function hasData() {
    return !is_null($this->data);
  }

  /**
   * Holds information about the operation.
   * Will return exception message if one is present and no other is specified.
   *
   * @return string
   */
  public function getMessage() {
    if (!$this->message && $this->hasException()) {
      return $this->getException()->getMessage();
    }
    return $this->message;
  }

  /**
   * Was there an exception?
   *
   * @return bool
   */
  public function hasException() {
    return !is_null($this->exception);
  }

  /**
   * Returns exception thrown by operation.
   *
   * @return \Exception|null
   */
  public function getException() {
    return $this->exception;
  }

}