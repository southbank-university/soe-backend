<?php
namespace Drupal\lsbu_feedback\Http;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Core\Mail\MailManager;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Site\Settings;

class FeedbackRequestHandler {

  private $data_manager;
  private $user;

  public function requestHandler(Request $request) {
    $data = json_decode($request->getContent(), TRUE);
    $user = \Drupal::currentUser();
    $this->data_manager = \Drupal::service('lsbu_feedback.datamanager');
    $this->data_manager->setSubject($data['subject']);
    $this->data_manager->setBody($data['message']);
    $this->data_manager->setUsermail($user->getEmail());
    $this->data_manager->setUsername($user->getDisplayName());

    if ($this->storeFeedback()) {
      $deanMail = Settings::get('soe_dean_email') ?: 'engcomms@lsbu.ac.uk';
      $this->sendMail($deanMail, $user->getEmail());
      $this->sendMail($user->getEmail(), $deanMail);
      return new Response(1);
    }

    return new Response(0);
  }

  private function storeFeedback() {
    $feedback = Node::create([
      'type' => 'feedback',
      'title' => t($this->data_manager->getSubject()),
      'body' => t($this->data_manager->getBody()),
      'status' => 1
    ]);
    if ($feedback->save()) {
      return True;
    }

    return False;
  }

  private function sendMail($to, $reply) {
    $mail_manager = \Drupal::service('plugin.manager.mail');
    $params['account'] = $to;
    $params['Reply-To'] = $reply ?: 'no-reply@lsbu.ac.uk';

    // If the custom site notification email has not been set, we use the site
    // default for this.
    if (empty($site_mail)) {
      $site_mail = \Drupal::config('system.site')->get('mail');
    }
    if (empty($site_mail)) {
      $site_mail = ini_get('sendmail_from');
    }

    $mail_manager->mail('lsbu_feedback', 'lsbu_feedback_mail', $to, 'en', $params, $site_mail);
  }

}
