<?php
namespace Drupal\lsbu_feedback\Manager;

use Drupal\Component\Utility\Html;

class DataManager {
  private $subject;
  private $body;
  private $usermail;
  private $username;

  /**
   * @param mixed $subject
   */
  public function setSubject($subject) {
    $this->subject = $subject;
  }

  /**
   * @param mixed $body
   */
  public function setBody($body) {
    $this->body = $body;
  }

  /**
   * @return mixed
   */
  public function getSubject() {
    return Html::escape($this->subject);
  }

  /**
   * @return mixed
   */
  public function getBody() {
    return Html::escape($this->body);
  }

  /**
   * @param mixed $usermail
   */
  public function setUsermail($usermail) {
    $this->usermail = $usermail;
  }

  /**
   * @return mixed
   */
  public function getUsermail() {
    return $this->usermail;
  }

  /**
   * @param mixed $username
   */
  public function setUsername($username) {
    $this->username = $username;
  }

  /**
   * @return mixed
   */
  public function getUsername() {
    return $this->username;
  }

}