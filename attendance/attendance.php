<?php
/**
 This file needs a configuration file placed together called config.php

 The contents must be:

 <?php

 return array(
   'key' => 'xxx',
   'ip' => '127.0.0.1',
   'database' => array(
     'host' => 'localhost',
     'username' => 'root',
     'password' => 'root',
     'dbname' => 'soe_attendance',
   ),
 );
 */

$config = require_once('config.php');

/**
 * Gets the request ip.
 */
function getRequestIp() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function attendanceOutput($value, $cancelled) {
  if ($cancelled) {
    return 'C';
  }
  return ($value) ? 'Y' : 'N';
}

function parseResults($results) {
  $list = array();
  foreach ($results as $result) {
    $result = (object) $result;
    $attendance = str_split(decbin($result->attend));
    while (count($attendance) < $result->weekcount) {
      array_unshift($attendance, 0);
    }
    $cancelled = str_split(decbin($result->cancelledweeks));
    while (count($cancelled) < $result->weekcount) {
      array_unshift($cancelled, 0);
    }
    $weeks_cancelled = count(array_filter($cancelled, create_function('$week','return $week == 1;')));

    $list[] = array(
      'room' => $result->room,
      'date' => $result->date,
      'time' => $result->time,
      'day' => $result->day,
      'semester' => $result->semester,
      'weekcount' => $result->weekcount,
      'moduleCode' => $result->unitcode,
      'moduleName' => $result->unitname,
      'attPercentage' => getAttendancePercentage($attendance, $weeks_cancelled, $result->weekcount),
      'attendance' => array_map('attendanceOutput', $attendance, $cancelled),
    );
  }
  return $list;
}

function getAttendancePercentage($num_attended, $num_cancelled, $expected) {
  $attended = 0;
  foreach ($num_attended as $a) {
    if ($a == '1') {
      $attended++;
    }
  }
  $percentage = $attended/($expected - $num_cancelled)*100;
  return round($percentage);
}



/*
FROM COMMENTS AT http://php.net/manual/en/function.json-encode.php

boukeversteegh at gmail dot com
6 years ago

For users of php 5.1.6 or lower, a native json_encode function.
This version handles objects, and makes proper distinction between [lists] and
{associative arrays}, mixed arrays work as well. It can handle newlines and
quotes in both keys and data.

This function will convert non-ascii symbols to "\uXXXX" format as does json_encode.

Besides that, it outputs exactly the same string as json_encode. Including UTF-8
encoded 2-, 3- and 4-byte characters. It is a bit faster than PEAR/JSON::encode,
but still slow compared to php 5.3's json_encode. It encodes any variable type
exactly as the original.

Relative speeds:
PHP json_encode: 1x
__json_encode: 31x
PEAR/JSON: 46x

NOTE: I assume the input will be valid UTF-8. I don't know what happens if your data
contains illegal Unicode sequences. I tried to make the code fast and compact.
*/
function __json_encode( $data ) {
    if( is_array($data) || is_object($data) ) {
        $islist = is_array($data) && ( empty($data) || array_keys($data) === range(0,count($data)-1) );

        if( $islist ) {
            $json = '[' . implode(',', array_map('__json_encode', $data) ) . ']';
        } else {
            $items = Array();
            foreach( $data as $key => $value ) {
                $items[] = __json_encode("$key") . ':' . __json_encode($value);
            }
            $json = '{' . implode(',', $items) . '}';
        }
    } elseif( is_string($data) ) {
        # Escape non-printable or Non-ASCII characters.
        # I also put the \\ character first, as suggested in comments on the 'addclashes' page.
        $string = '"' . addcslashes($data, "\\\"\n\r\t/" . chr(8) . chr(12)) . '"';
        $json    = '';
        $len    = strlen($string);
        # Convert UTF-8 to Hexadecimal Codepoints.
        for( $i = 0; $i < $len; $i++ ) {

            $char = $string[$i];
            $c1 = ord($char);

            # Single byte;
            if( $c1 <128 ) {
                $json .= ($c1 > 31) ? $char : sprintf("\\u%04x", $c1);
                continue;
            }

            # Double byte
            $c2 = ord($string[++$i]);
            if ( ($c1 & 32) === 0 ) {
                $json .= sprintf("\\u%04x", ($c1 - 192) * 64 + $c2 - 128);
                continue;
            }

            # Triple
            $c3 = ord($string[++$i]);
            if( ($c1 & 16) === 0 ) {
                $json .= sprintf("\\u%04x", (($c1 - 224) <<12) + (($c2 - 128) << 6) + ($c3 - 128));
                continue;
            }

            # Quadruple
            $c4 = ord($string[++$i]);
            if( ($c1 & 8 ) === 0 ) {
                $u = (($c1 & 15) << 2) + (($c2>>4) & 3) - 1;

                $w1 = (54<<10) + ($u<<6) + (($c2 & 15) << 2) + (($c3>>4) & 3);
                $w2 = (55<<10) + (($c3 & 15)<<6) + ($c4-128);
                $json .= sprintf("\\u%04x\\u%04x", $w1, $w2);
            }
        }
    } else {
        # int, floats, bools, null
        $json = strtolower(var_export( $data, true ));
    }
    return $json;
}

// Validation.
if (getRequestIp() != $config['ip']) {
  die ('403 Forbidden. Invalid request.');
}

if (!isset($_GET['key']) || $_GET['key'] != $config['key']) {
  die ('403 Forbidden. Invalid request.');
}

if (!isset($_GET['email'])) {
  die ('403 Forbidden. Invalid request.');
}

$resource = mysql_connect(
  $config['database']['host'],// ?: 'localhost',
  $config['database']['username'],
  $config['database']['password']);

mysql_select_db($config['database']['dbname'], $resource);

$query = <<<SQL
SELECT r.unitcode, r.weekcount, r.room, r.date, rt.attend, r.time, r.day, r.semester, rn.unitname, r.cancelledweeks
  FROM R10_registers r, R10_register_ticks rt, R10_studentnames rs, R10_unitnames rn
WHERE rs.email = '%s'
  AND rt.studentnumber = rs.studentnumber
  AND r.regid = rt.regid
  AND r.unitcode = rn.unitcode;
SQL;

$query = sprintf($query, mysql_real_escape_string($_GET['email']));

$result = mysql_query($query, $resource);
if (!$result) {
  __json_encode(array());
  die;
}

$aggregate = array();
while ($row = mysql_fetch_assoc($result)) {
  $aggregate[] = $row;
}

$list = parseResults($aggregate);
print __json_encode($list);

//phpinfo();

