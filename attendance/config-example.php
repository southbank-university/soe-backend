<?php
/**
 * This file must be called config.php
 */

return array(
  'key' => 'xxx',
  'ip' => '127.0.0.1',
  'database' => array(
    'host' => 'localhost',
    'username' => 'root',
    'password' => 'root',
    'dbname' => 'soe_attendance',
  ),
);
